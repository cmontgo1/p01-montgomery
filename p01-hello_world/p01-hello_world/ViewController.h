//
//  ViewController.h
//  p01-hello_world
//
//  Created by Montgomery Chelsea on 1/31/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

//Label
@property(nonatomic, strong) IBOutlet UILabel *hello_world;
//Button
@property(nonatomic, strong) IBOutlet UIButton *push_me;

//Action for clicking button
-(IBAction)click:(id)sender;

//Global variable to determine what number click we are on (bad design I know)
extern int clickCounter;

@end


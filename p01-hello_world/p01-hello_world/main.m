//
//  main.m
//  p01-hello_world
//
//  Created by Montgomery Chelsea on 1/31/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  AppDelegate.h
//  p01-hello_world
//
//  Created by Montgomery Chelsea on 1/31/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


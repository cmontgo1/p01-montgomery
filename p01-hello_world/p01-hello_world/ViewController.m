//
//  ViewController.m
//  p01-hello_world
//
//  Created by Montgomery Chelsea on 1/31/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize hello_world;
@synthesize push_me;

//begin the click counter
int clickCounter = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)click:(id)sender
{

    //switch based on num clicks
    switch (clickCounter)
    {
        case 0:
            [hello_world setText:@"Chelsea was here!"];
            [self.push_me setAttributedTitle:nil forState:UIControlStateNormal];
            [self.push_me setTitle:@"Click Me Again" forState:UIControlStateNormal];
            break;
        case 1:
            [hello_world setText:@"You can stop clicking now."];
            [push_me setTitle:@"Don't Listen, Click Again" forState:UIControlStateNormal];
            break;
        case 2:
            [hello_world setText:@"Okay seriously, stop."];
            [push_me setTitle:@"Click Me! Do It!" forState:UIControlStateNormal];
            break;
        case 3:
            [hello_world setText:@"There's nothing more to see."];
            [push_me setTitle:@"He's Lying, Click Me!" forState:UIControlStateNormal];
            break;
        case 4:
            [hello_world setText:@"...."];
            [push_me setTitle:@"Click Click Click" forState:UIControlStateNormal];
            break;
        case 5:
            [hello_world setText:@"Why are you still clicking?"];
            [push_me setTitle:@"One More Time!" forState:UIControlStateNormal];
            break;
        case 6:
            [hello_world setText:@"Okay enough switch statements."];
            [push_me setTitle:@"Click To Start Over" forState:UIControlStateNormal];
            break;
        case 7:
            [hello_world setText:@"Hello World!"];
            [push_me setTitle:@"Push Me" forState:UIControlStateNormal];
            break;
        
    }
    
    //update clicks
    clickCounter = (clickCounter+1)%8;
        
}

@end
